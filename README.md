# Official pipes registry

## What are official pipes?

This repository contains a curated set of pipes hosted on Bitbucket and displayed in Bitbucket Pipelines UI. They are built to:

- Simplify configuring your pipeline by integrating with third-party vendors such as AWS, GCP, Azure, Snyk, JFrog, etc.
- Exemplify pipe best practices and provide clear and concise documentation to serve as a reference for other pipe authors.
- Showcase a set of pipes that are officially maintained: Atlassian works in collaboration with third-party vendors to ensure bugfixes and security updates are applied in a timely manner.

While it's recommended to have software authors maintaining their corresponding official Pipes, this is not a strict requirement. Creating and maintaining pipes is a public process, anyone can provide feedback, contribute code and suggest process changes.

## What do you mean by _official_?

In many cases, the pipes in this repository are not only supported but also *maintained* directly by the relevant vendors.

Others have been developed in collaboration with the company and reviewed by them.

## Contributing to the official pipes

We'd love to have your contribution to this project. If you are a vendor and you would like to get involved in the project, please read the following contribution guidelines. 
We hope to make these instructions as simple as possible, but if you'd like help with the process or you have an issue or question, let us know on [pipelines-feedback@atlassian.com](mailto:pipelines-feedback@atlassian.com).

### How to contribute a pipe

#### Step 1: Develop and test your pipe and make sure it is publicly available
Ensure you follow the _complete_ pipe requirements in the [How to Write a pipe docs](https://confluence.atlassian.com/bitbucket/how-to-write-a-pipe-for-bitbucket-pipelines-966051288.html).

#### Step 2: Fork this repository
More details in the [_How to fork a repository guide_](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).

#### Step 3: Add your pipe manifest file
Create a `.yml` file in `pipes` folder in this repository. 

```yaml
name: Pipe name
description: Deploy your code to Firebase.
repositoryPath: atlassian/firebase-deploy
version: 0.2.1
vendor:
  name: Google
  website: https://www.google.com/
maintainer:
  name: Atlassian
  website: https://www.atlassian.com/
```

Note: Use the id of your pipe to name the file. For instance, in the previous example authors should use `firebase-deploy.yml`. 

#### Step 4: Ensure your pipe follows technical and documentation requirements

Ensure you comply with the Review Guidelines listed below.

We provide a script that you can run in local to verify that your pipe is eligible to be submitted:

```python
python build.py 'pipes/my-pipe-name.yml'
```

#### Step 5: Submit a pull request

Finally, create and submit a pull request with the required changes. A repository maintainer will review the pipe submission and verify the technical and non-technical requirements. Once everything looks good and the pull request is merged the new pipe will appear in the UI.


### Review guidelines

We review each proposed pipe to ensure that it meets a minimum standard of quality and maintainability as we want to ensure the best experience for Bitbucket Pipelines users. 
We are aware that the quality standard is hard to define so we are also adhering to the [docker best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/) where appropriate.

Before adding a pull-request you will need to make sure you comply with the following checklist:

- You are vendor who agrees to commit to maintain the pipe including security fixes and timely updates as appropriate.
- You created a public repository that contains the source code and documentation. See [How to Write a pipe docs](https://confluence.atlassian.com/bitbucket/how-to-write-a-pipe-for-bitbucket-pipelines-966051288.html) for more details.
- Your pipe is well tested and using proper CI/CD to automate the process.
- You repository contains a `README.md` file with a [predefined structure](https://confluence.atlassian.com/bitbucket/how-to-write-a-pipe-for-bitbucket-pipelines-966051288.html#HowtowriteapipeforBitbucketPipelines-Step6-WriteREADME.md-yourreadmefile). 
- You are tagging your repository when releasing, using [sematic versioning](https://semver.org/).
- Your resulting docker image is less than 1GB.
- You offer a support channel so that users of your pipe can submit bug reports and feature requests.

#### Maintainership & commitment

We expect and require commitment to maintain the pipe, including security updates and bug fixes as appropriate

#### Repeatability

Users rerunning the same pipe should result in the same version of the image being built. For that, as a pipe author we recommend relying on fixed versions for your libraries and docker images.

- Bad:

```Dockerfile
FROM alpine:latest
COPY pipe /
ENTRYPOINT ["/pipe.sh"]
```

- Good: 

```Dockerfile
FROM alpine:3.8
COPY pipe /
ENTRYPOINT ["/pipe.sh"]
```

#### Security

The `Dockerfile` should be written in a way to help mitigate man-in-the-middle attacks during the build. For that, we recommend embedding checksums directly in the `Dockerfile` if PGP signing is not provided. 

- Bad:

```Dockerfile
FROM alpine:3.9
RUN apk update && apk add curl
RUN curl -fSL -o ruby.tar.gz "https://cache.ruby-lang.org/pub/ruby/2.6/ruby-2.6.2.tar.gz"
```

- Good: 

```Dockerfile
FROM alpine:3.9
RUN apk update && apk add curl
ENV RUBY_DOWNLOAD_SHA256 a0405d2bf2c2d2f332033b70dff354d224a864ab0edd462b7a413420453b49ab
RUN curl -fSL -o ruby.tar.gz "https://cache.ruby-lang.org/pub/ruby/2.6/ruby-2.6.2.tar.gz" \
    && echo "$RUBY_DOWNLOAD_SHA256 *ruby.tar.gz" | sha256sum -c -
```


#### Performance

Build the smallest image possible. This will decrease download times, cold start times, and disk usage for Bitbucket Pipelines users. As it affects costs, please pay a lot of attention to this.

- Bad:

```Dockerfile
FROM ubuntu:18.04
COPY pipe /
ENTRYPOINT ["/pipe.sh"]
```

- Good: 

```Dockerfile
FROM alpine:3.8
COPY pipe /
ENTRYPOINT ["/pipe.sh"]
```
